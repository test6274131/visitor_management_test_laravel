<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            [
                'id'             => 1,
                'name'           => 'Admin',
                'phone'          => null,
                'email'          => 'admin@admin.com',
                'password'       => bcrypt('password'),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 2,
                'name'           => 'Staff A',
                'phone'          => null,
                'email'          => 'staffA@gmail.com',
                'password'       => bcrypt('password'),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
            [
                'id'             => 3,
                'name'           => 'Visitor A',
                'phone'          => '65123456789',
                'email'          => null,
                'password'       => null,
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
        ];
        User::insert($users);
    }
}
