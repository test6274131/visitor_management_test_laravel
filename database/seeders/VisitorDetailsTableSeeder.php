<?php

namespace Database\Seeders;

use App\Models\VisitorDetail;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class VisitorDetailsTableSeeder extends Seeder
{
    public function run()
    {
        $visitor = [
            [
                'id'             => 1,
                'purpose'        => 'Meet friends',
                'is_walkin'      => 1,
                'user_id'        => 3,
                'recorder_id'    => 1,
                'check_in_at'    => Carbon::now(),
                'created_at'     => Carbon::now(),
                'updated_at'     => Carbon::now(),
            ],
        ];
        VisitorDetail::insert($visitor);
    }
}
