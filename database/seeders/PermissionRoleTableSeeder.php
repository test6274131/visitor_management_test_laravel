<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = Permission::all();
        Role::findOrFail(1)->permissions()->sync($permissions->pluck('id'));
        $staff_permissions = $permissions->filter(function ($permission) {
            return substr($permission->title, 0, 15) == 'visitor_detail_';
        });
        Role::findOrFail(2)->permissions()->sync($staff_permissions->pluck('id'));
    }
}
