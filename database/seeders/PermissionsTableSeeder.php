<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'user_create',
            ],
            [
                'id'    => 3,
                'title' => 'user_edit',
            ],
            [
                'id'    => 4,
                'title' => 'user_show',
            ],
            [
                'id'    => 5,
                'title' => 'user_delete',
            ],
            [
                'id'    => 6,
                'title' => 'user_access',
            ],
            [
                'id'    => 7,
                'title' => 'permission_management_access',
            ],
            [
                'id'    => 8,
                'title' => 'permission_create',
            ],
            [
                'id'    => 9,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 10,
                'title' => 'permission_show',
            ],
            [
                'id'    => 11,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 12,
                'title' => 'permission_access',
            ],
            [
                'id'    => 13,
                'title' => 'role_management_access',
            ],
            [
                'id'    => 14,
                'title' => 'role_create',
            ],
            [
                'id'    => 15,
                'title' => 'role_edit',
            ],
            [
                'id'    => 16,
                'title' => 'role_show',
            ],
            [
                'id'    => 17,
                'title' => 'role_delete',
            ],
            [
                'id'    => 18,
                'title' => 'role_access',
            ],
            [
                'id'    => 19,
                'title' => 'visitor_detail_management_access',
            ],
            [
                'id'    => 20,
                'title' => 'visitor_detail_create',
            ],
            [
                'id'    => 21,
                'title' => 'visitor_detail_edit',
            ],
            [
                'id'    => 22,
                'title' => 'visitor_detail_show',
            ],
            [
                'id'    => 23,
                'title' => 'visitor_detail_delete',
            ],
            [
                'id'    => 24,
                'title' => 'visitor_detail_access',
            ],
        ];

        Permission::insert($permissions);
    }
}
