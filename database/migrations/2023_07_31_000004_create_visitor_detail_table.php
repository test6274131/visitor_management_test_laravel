<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('visitor_details', function (Blueprint $table) {
            $table->id();
            $table->string('purpose');
            $table->boolean('is_walkin')->default(false);
            $table->string('vehicle_number')->nullable();
            $table->datetime('check_in_at');
            $table->datetime('check_out_at')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_id_fk_20230731002')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('recorder_id')->nullable();
            $table->foreign('recorder_id', 'recorder_id_fk_20230731001')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('visitor_details');
    }
};
