<a class="btn btn-success" href="{{ route('admin.' . $routePart . '.show', $row->id) }}">
    {{ trans('global.view') }}
</a>
<a class="btn btn-success" href="{{ route('admin.' . $routePart . '.edit', $row->id) }}">
    {{ trans('global.update') }}
</a>
<form class="d-inline" method="POST" action="{{ route('admin.' . $routePart . '.destroy', $row->id) }}">
    @method('DELETE')    
    @csrf
    <button class="btn btn-danger" type="submit">
        <i class="bi bi-trash3-fill"></i>
    </button>
</form>