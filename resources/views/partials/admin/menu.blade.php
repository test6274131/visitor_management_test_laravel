<div class="flex-shrink-0 p-3 bg-white" style="width: 280px;">
    <a href="{{ route('admin.home') }}" class="d-flex align-items-center pb-3 mb-3 link-dark text-decoration-none border-bottom">
        <svg class="bi me-2" width="30" height="24"><use xlink:href="#bootstrap"></use></svg>
        <span class="fs-5 fw-semibold">Menu</span>
    </a>
    <ul class="list-unstyled ps-0">
        @permit('user_management_access')
        <li class="mb-1">
            <button class="btn btn-toggle align-items-center rounded collapsed" data-bs-toggle="collapse" data-bs-target="#user-collapse" aria-expanded="false">
                {{ trans('cruds.user.title') }}
            </button>
            <div class="collapse ps-3" id="user-collapse">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                    @permit('user_access')
                    <li><a href="{{ route('admin.users.index') }}" class="link-dark rounded">{{ trans('cruds.user.name') }} {{ trans('global.list') }}</a></li>
                    @endpermit
                    @permit('user_create')
                    <li><a href="{{ route('admin.users.create') }}" class="link-dark rounded">{{ trans('global.create') }} {{ trans('cruds.user.name') }}</a></li>
                    @endpermit
                </ul>
            </div>
        </li>
        @endpermit
        @permit('visitor_detail_management_access')
        <li class="mb-1">
            <button class="btn btn-toggle align-items-center rounded collapsed" data-bs-toggle="collapse" data-bs-target="#user-detail-collapse" aria-expanded="false">
                {{ trans('cruds.visitor.title') }}
            </button>
            <div class="collapse ps-3" id="user-detail-collapse">
                <ul class="btn-toggle-nav list-unstyled fw-normal pb-1 small">
                    @permit('visitor_detail_access')
                    <li><a href="{{ route('admin.visitor.index') }}" class="link-dark rounded">{{ trans('cruds.visitor.name') }} {{ trans('global.list') }}</a></li>
                    @endpermit
                    @permit('visitor_detail_create')
                    <li><a href="{{ route('admin.visitor.create') }}" class="link-dark rounded">{{ trans('global.create') }} {{ trans('cruds.visitor.name') }}</a></li>
                    @endpermit
                </ul>
            </div>
        </li>
        @endpermit
        <li class="mb-1">
            <form action="{{ route('auth.logout') }}" method="POST" style="display: inline-block;">
                @csrf
                <input type="submit" class="btn btn-link" value="{{ trans('global.logout') }}">
            </form>
        </li>
    </ul>
</div>