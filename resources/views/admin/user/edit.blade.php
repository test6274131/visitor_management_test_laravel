@extends('layouts.admin')

@section('body')
    <div class="card tw-rounded-2xl tw-shadow-md tw-border-white animation-float-in">
        <div class="card-header tw-text-base tw-font-bold border-radius-top">
            {{ trans('global.update') }} {{ trans('cruds.user.name') }}
        </div>
    
        <div class="card-body">
            <form method="POST" action="{{ route('admin.users.update', $object->id) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group mb-4">
                    <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                    <input class="form-control" type="text" name="name" id="name" value="{{ $object->name }}" required>
                    @if($errors->has('name'))
                        <div class="text-danger">
                            *{{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                    <input class="form-control" type="text" name="email" id="email" value="{{ $object->email }}" required>
                    @if($errors->has('email'))
                        <div class="text-danger">
                            *{{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.update') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection