@extends('layouts.admin')

@section('body')
    <div class="card tw-rounded-2xl tw-shadow-md tw-border-white animation-float-in">
        <div class="card-header tw-text-base tw-font-bold border-radius-top">
            User Details
        </div>
    
        <div class="card-body">
            <div class="form-group">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>
                                ID
                            </th>
                            <td>
                                {{ $object->id }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Name
                            </th>
                            <td>
                                {{ $object->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Email
                            </th>
                            <td>
                                {{ $object->email }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                Create At
                            </th>
                            <td>
                                {{ $object->created_at }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <a class="btn btn-light mt-2" href="{{ route('admin.users.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection