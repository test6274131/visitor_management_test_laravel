@extends('layouts.admin')

@section('body')
    <div class="card tw-rounded-2xl tw-shadow-md tw-border-white animation-float-in">
        <div class="card-header tw-text-base tw-font-bold border-radius-top">
            {{ trans('global.create') }} {{ trans('cruds.user.name') }}
        </div>
    
        <div class="card-body">
            <form method="POST" action="{{ route('admin.users.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group mb-4">
                    <label class="required" for="name">{{ trans('cruds.user.fields.name') }}</label>
                    <input class="form-control" type="text" name="name" id="name" required>
                    @if($errors->has('name'))
                        <div class="text-danger">
                            *{{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <label class="required" for="email">{{ trans('cruds.user.fields.email') }}</label>
                    <input class="form-control" type="text" name="email" id="email" required>
                    @if($errors->has('email'))
                        <div class="text-danger">
                            *{{ $errors->first('email') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <label class="required" for="password">{{ trans('cruds.user.fields.password') }}</label>
                    <input class="form-control" type="password" name="password" id="email" required>
                    @if($errors->has('password'))
                        <div class="text-danger">
                            *{{ $errors->first('password') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.submit') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
