@extends('layouts.admin')

@section('body')
    <div class="card tw-rounded-2xl tw-shadow-md tw-border-white animation-float-in">
        <div class="card-header tw-text-base tw-font-bold border-radius-top">
            {{ trans('global.create') }} {{ trans('cruds.visitor.name') }}
        </div>
    
        <div class="card-body">
            <form method="POST" action="{{ route('admin.visitor.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group mb-4">
                    <label class="required" for="name">{{ trans('cruds.visitor.fields.name') }}</label>
                    <input class="form-control" type="text" name="name" id="name" required>
                    @if($errors->has('name'))
                        <div class="text-danger">
                            *{{ $errors->first('name') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <label class="required" for="phone">{{ trans('cruds.visitor.fields.phone') }}</label>
                    <input class="form-control" type="text" name="phone" id="phone" required>
                    @if($errors->has('phone'))
                        <div class="text-danger">
                            *{{ $errors->first('phone') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <label class="required" for="purpose">{{ trans('cruds.visitor.fields.purpose') }}</label>
                    <input class="form-control" type="text" name="purpose" id="purpose" required>
                    @if($errors->has('purpose'))
                        <div class="text-danger">
                            *{{ $errors->first('purpose') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="is_walkin" id="is_walkin" value="1">
                        <label class="form-check-label" for="approved">{{ trans('cruds.visitor.fields.is_walkin') }}</label>
                    </div>
                    @if($errors->has('is_walkin'))
                        <div class="text-danger">
                            *{{ $errors->first('is_walkin') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <label class="required" for="vehicle_number">{{ trans('cruds.visitor.fields.vehicle_number') }}</label>
                    <input class="form-control" type="text" name="vehicle_number" id="vehicle_number">
                    @if($errors->has('vehicle_number'))
                        <div class="text-danger">
                            *{{ $errors->first('vehicle_number') }}
                        </div>
                    @endif
                </div>
                <div class="form-group mb-4">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.submit') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
