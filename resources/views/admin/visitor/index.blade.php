@extends('layouts.admin')

@section('body')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.visitor.create') }}">
                {{ trans('global.create') }} {{ trans('cruds.visitor.name') }}
            </a>
        </div>
    </div>

    <div class="card tw-rounded-2xl tw-shadow-md tw-border-white animation-float-in">
        <div class="card-header tw-text-lg tw-font-bold border-radius-top">
            {{ trans('cruds.visitor.title') }}
        </div>

        <div class="card-body">
            <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Visitor">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.visitor.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.visitor.fields.phone') }}
                        </th>
                        <th>
                            {{ trans('cruds.visitor.fields.is_walkin') }}
                        </th>
                        <th>
                            {{ trans('cruds.visitor.fields.vehicle_number') }}
                        </th>
                        <th>
                            {{ trans('cruds.visitor.fields.check_in_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.visitor.fields.check_out_at') }}
                        </th>
                        <th>
                            {{ trans('global.action') }}
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="is_walkin_checkbox" type="checkbox" id="is_walkin_checkbox" value="1">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection


@section('script')
@parent
<script>
    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

        let dtOverrideGlobals = {
            buttons: dtButtons,
            info: false,
            processing: true,
            serverSide: true,
            retrieve: true,
            aaSorting: [],
            ajax: "{{ route('admin.visitor.index') }}",
            columns: [
                { data: 'name', name: 'name' },
                { data: 'phone', name: 'phone' },
                { data: 'is_walkin', name: 'is_walkin' },
                { data: 'vehicle_number', name: 'vehicle_number' },
                { data: 'check_in_at', name: 'check_in_at' },
                { data: 'check_out_at', name: 'check_out_at' },
                { data: 'actions', name: '{{ trans("global.actions") }}', "orderable": false }
            ],
            orderCellsTop: true,
            order: [[ 1, 'desc' ]],
            pageLength: 100,
        };
        let table = $('.datatable-Visitor').DataTable(dtOverrideGlobals);


        $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
            $($.fn.dataTable.tables(true)).DataTable()
                .columns.adjust();
        });
            
        let visibleColumnsIndexes = null;
        $('.datatable thead').on('input', '.search', function () {
            let strict = $(this).attr('strict') || false
            let value = strict && this.value ? "^" + this.value + "$" : this.value

            let index = $(this).parent().index()
            if (visibleColumnsIndexes !== null) {
                index = visibleColumnsIndexes[index]
            }

            table
                .column(index)
                .search(value, strict)
                .draw()
        });

        $(".is_walkin_checkbox").on("change", function(e) {
            let index = $(this).parent().index()
            table
                .column(index)
                .search($('#is_walkin_checkbox').is(':checked') ? "{{ trans('global.yes') }}" : "{{ trans('global.no') }}")
                .draw()
        });
    });

</script>
@endsection