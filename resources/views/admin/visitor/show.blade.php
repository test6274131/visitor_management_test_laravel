@extends('layouts.admin')

@section('body')
    <div class="card tw-rounded-2xl tw-shadow-md tw-border-white animation-float-in">
        <div class="card-header tw-text-base tw-font-bold border-radius-top">
            {{ trans('cruds.visitor.name') }}
        </div>
    
        <div class="card-body">
            <div class="form-group">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>
                                {{ trans('global.id') }}
                            </th>
                            <td>
                                {{ $object->id }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.name') }}
                            </th>
                            <td>
                                {{ $object->user->name }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.phone') }}
                            </th>
                            <td>
                                {{ $object->user->phone }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.purpose') }}
                            </th>
                            <td>
                                {{ $object->purpose }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.is_walkin') }}
                            </th>
                            <td>
                                {{ $object->is_walkin == 1 ? trans('global.yes') : trans('global.no') }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.vehicle_number') }}
                            </th>
                            <td>
                                {{ $object->vehicle_number }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.check_in_at') }}
                            </th>
                            <td>
                                {{ $object->check_in_at }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.check_out_at') }}
                            </th>
                            <td>
                                {{ $object->check_out_at }}
                            </td>
                        </tr>
                        <tr>
                            <th>
                                {{ trans('cruds.visitor.fields.recorder') }}
                            </th>
                            <td>
                                {{ $object->recorder->name }}
                            </td>
                        </tr>
                    </tbody>
                </table>
                @if(!$object->check_out_at)
                <form class="d-inline" method="POST" action="{{ route('admin.visitor.checkout', $object->id) }}">
                    @csrf
                    <button class="btn btn-danger" type="submit">
                        {{ trans('cruds.visitor.fields.check_out') }}
                    </button>
                </form>
                @endif
                <div class="form-group">
                    <a class="btn btn-light mt-2" href="{{ route('admin.visitor.index') }}">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection