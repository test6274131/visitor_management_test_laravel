@extends('layouts.app')

@section('body')
<div class="row justify-content-center">
    <div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6">
        <div class="card">
            <div class="card-body">
                <h4 class="tw-text-xl tw-font-medium mb-3">{{ trans('global.login') }}</h4>

                <form method="POST" action="{{ route('auth.user_login') }}">
                    @csrf

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>

                        <input id="email" name="email" type="text"
                            class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required
                            autocomplete="email" autofocus placeholder="{{ trans('global.email') }}"
                            value="{{ old('email', null) }}">

                        @if($errors->has('email'))
                        <div class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </div>
                        @endif
                    </div>

                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-lock"></i></span>
                        </div>

                        <input id="password" name="password" type="password"
                            class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required
                            placeholder="{{ trans('global.password') }}">

                        @if($errors->has('password'))
                        <div class="invalid-feedback">
                            {{ $errors->first('password') }}
                        </div>
                        @endif
                    </div>

                    <div class="flex row mb-3">
                        <div class="col-6">
                            <div class="input-group">
                                <div class="form-check checkbox">
                                    <input class="form-check-input" name="remember" type="checkbox" id="remember" />
                                    <label class="form-check-label" for="remember">
                                        {{ trans('global.remember_me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($errors->has('account'))
                        <div class="text-danger">
                            *{{ $errors->first('account') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-12">
                            <button type="submit" class="btn tw-bg-blue-900 tw-text-white tw-bg-blue hvr-border-btn" style="min-width: 100%">
                                {{ trans('global.login') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection