<?php

return [
    'login'                          => 'Login',
    'remember_me'                    => 'Remember Me',
    'email'                          => 'Email',
    'password'                       => 'Password',
    'invalid_email_or_password'      => 'Invalid email or password',
    'logout'                         => 'Logout',
    'view'                           => 'View',
    'create'                         => 'Create',
    'update'                         => 'Update',
    'list'                           => 'List',
    'submit'                         => 'Submit',
    'no.'                            => 'No.',
    'id'                             => 'ID',
    'action'                         => 'Action',
    'yes'                            => 'Yes',
    'no'                             => 'No',
    'search'                         => 'Search',
];
