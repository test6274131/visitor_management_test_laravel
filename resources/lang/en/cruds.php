<?php

return [
    'user' => [
        'title'          => 'User Management',
        'name'           => 'User',
        'fields'         => [
            'name'                         => 'Name',
            'name_helper'                  => ' ',
            'phone'                        => 'Phone Number',
            'phone_helper'                 => ' ',
            'email'                        => 'Email',
            'email_helper'                 => ' ',
            'password'                     => 'Password',
            'password_helper'              => ' ',
            'created_at'                   => 'Created at',
            'created_at_helper'            => ' ',
            'updated_at'                   => 'Updated at',
            'updated_at_helper'            => ' ',
            'deleted_at'                   => 'Deleted at',
            'deleted_at_helper'            => ' ',            
        ],
    ],
    'visitor' => [
        'title'          => 'Visitor Management',
        'board'          => 'Visitor Board',
        'name'           => 'Visitor',
        'fields'         => [
            'name'                         => 'Name',
            'name_helper'                  => ' ',
            'phone'                        => 'Phone Number',
            'phone_helper'                 => ' ',
            'purpose'                      => 'Purpose',
            'purpose_helper'               => ' ',
            'vehicle_number'               => 'Vehicle Number',
            'vehicle_number_helper'        => ' ',
            'is_walkin'                    => 'Walk in',
            'is_walkin_helper'             => ' ',
            'check_in_at'                  => 'Check in at',
            'check_in_at_helper'           => ' ',
            'check_out'                    => 'Check out',
            'check_out_helper'             => ' ',
            'check_out_at'                 => 'Check out at',
            'check_out_at_helper'          => ' ',
            'recorder'                     => 'Recorder',
            'recorder_helper'              => ' ',
            'created_at'                   => 'Created at',
            'created_at_helper'            => ' ',
            'updated_at'                   => 'Updated at',
            'updated_at_helper'            => ' ',
            'deleted_at'                   => 'Deleted at',
            'deleted_at_helper'            => ' ',            
        ],
        'validation' => [
            'is_walkin_verhicle_number_required'    => 'Walk in or verhicle number must fill in one'
        ]
    ],
];
