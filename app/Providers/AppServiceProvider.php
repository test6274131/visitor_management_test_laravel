<?php

namespace App\Providers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Blade::if('permit', function (null|string|Permission $permission) {
            if ($permission == null) {
                return false;
            }

            foreach(auth()->user()->roles as $id => $role){
                $permissionFound = $role->permissions->filter(function ($p) use ($permission) {
                    return $p->title == $permission;
                })->values();

                if(!$permissionFound->isEmpty()){
                    return true;
                }
            }

            return false;
        });
    }
}
