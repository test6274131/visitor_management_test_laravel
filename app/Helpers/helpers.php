<?php

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Gate;


if (!function_exists('actionDenied')) {
    function actionDenied(string|Permission $permission): bool {
        if ($permission == null) {
            return false;
        }

        foreach(auth()->user()->roles as $id => $role){
            $permissionFound = $role->permissions->filter(function ($p) use ($permission) {
                return $p->title == $permission;
            })->values();

            if(!$permissionFound->isEmpty()){
                return false;
            }
        }

        return true;
    }
}