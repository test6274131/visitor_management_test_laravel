<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VisitorDetail extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'visitor_details';

    protected $dates = [
        'check_in_at',
        'check_out_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'purpose',
        'is_walkin',
        'vehicle_number',
        'check_in_at',
        'check_out_at',
        'user_id',
        'recorder_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function recorder()
    {
        return $this->belongsTo(User::class, 'recorder_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
