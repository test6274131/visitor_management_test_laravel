<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Models\VisitorDetail;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class VisitorController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    
    public function index(Request $request){
        abort_if(actionDenied('visitor_detail_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = VisitorDetail::all();
            $table = Datatables::of($query);

            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $routePart = 'visitor';
                $viewGate = $routePart . '_show';
                $editGate = $routePart . '_edit';
                $deleteGate = $routePart . '_delete';

                return view('partials.admin.table_actions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'routePart',
                    'row'
                ));
            });

            $table->editColumn('name', function ($row) {
                return $row->user->name ? $row->user->name : '';
            });
            $table->editColumn('phone', function ($row) {
                return $row->user->phone ? $row->user->phone : '';
            });
            $table->editColumn('is_walkin', function ($row) {
                return $row->is_walkin ? trans('global.yes') : trans('global.no');
            });
            $table->editColumn('vehicle_number', function ($row) {
                return $row->vehicle_number ? $row->vehicle_number : '';
            });
            $table->editColumn('check_in_at', function ($row) {
                return $row->check_in_at ? $row->check_in_at : '';
            });
            $table->editColumn('check_out_at', function ($row) {
                return $row->check_out_at ? $row->check_out_at : '';
            });
            $table->rawColumns(['actions']);

            return $table->make(true);
        }

        return view('admin.visitor.index');
    }

    public function create(){
        abort_if(actionDenied('visitor_detail_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.visitor.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => ['required'],
            'phone' => ['required', 'numeric', 'digits_between:10,15'],
            'purpose' => ['required'],
        ]);

        if(($request->is_walkin && $request->vehicle_number) || 
            (!$request->is_walkin && !$request->vehicle_number)
        ){
            return back()->withErrors([
                'is_walkin'      => trans('cruds.visitor.validation.is_walkin_verhicle_number_required'),
                'vehicle_number' => trans('cruds.visitor.validation.is_walkin_verhicle_number_required'),
            ]);
        }

        $user = User::create($request->all());
        $user->roles()->sync(Role::find(3));

        VisitorDetail::create([
            ...$request->all(),
            'check_in_at' => Carbon::now(),
            'user_id' => $user->id,
            'recorder_id' => auth()->user()->id,
        ]);

        return redirect()->route('admin.visitor.index');
    }

    public function show(Request $request, $id){
        abort_if(actionDenied('visitor_detail_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $object = VisitorDetail::find($id);

        return view('admin.visitor.show', compact('object'));
    }

    public function edit(Request $request, $id){
        abort_if(actionDenied('visitor_detail_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $object = VisitorDetail::find($id);

        return view('admin.visitor.edit', compact('object'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => ['required'],
            'phone' => ['required', 'numeric', 'digits_between:10,15'],
            'purpose' => ['required'],
        ]);

        if(($request->is_walkin && $request->vehicle_number) || 
            (!$request->is_walkin && !$request->vehicle_number)
        ){
            return back()->withErrors([
                'is_walkin'      => trans('cruds.visitor.validation.is_walkin_verhicle_number_required'),
                'vehicle_number' => trans('cruds.visitor.validation.is_walkin_verhicle_number_required'),
            ]);
        }

        $object = VisitorDetail::find($id);
        $object->update([
            ...$request->all(),
            'is_walkin' => $request->is_walkin ?? false
        ]);

        $object->user->update($request->all());

        return redirect()->route('admin.visitor.index');
    }

    public function checkout(Request $request, $id){
        $object = VisitorDetail::find($id);
        $object->update([
            'check_out_at' => Carbon::now(),
        ]);

        return redirect()->route('admin.visitor.index');
    }

    public function destroy(Request $request, $id){
        abort_if(actionDenied('visitor_detail_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $object = User::find($id);
        $object->delete();

        return back();
    }
}
