<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;
    
    public function index(){
        return view('admin.home');
    }
}
