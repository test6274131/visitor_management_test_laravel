<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;

    
    public function index(Request $request){
        abort_if(actionDenied('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = User::whereHas('roles', function ($roles) {
                return $roles->where('title', 'Staff');
            });
            $table = Datatables::of($query);

            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $routePart = 'users';
                $viewGate = $routePart . '_show';
                $editGate = $routePart . '_edit';
                $deleteGate = $routePart . '_delete';

                return view('partials.admin.table_actions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'routePart',
                    'row'
                ));
            });

            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : '';
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : '';
            });
            $table->rawColumns(['actions']);

            return $table->make(true);
        }

        return view('admin.user.index');
    }

    public function create(){
        abort_if(actionDenied('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.user.create');
    }

    public function store(Request $request){
        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
        
        $user = User::create([
            ...$request->all(),
            'password' => bcrypt($request->password),
        ]);

        $user->roles()->sync(Role::find(2));

        return redirect()->route('admin.users.index');
    }

    public function show(Request $request, $id){
        abort_if(actionDenied('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $object = User::find($id);

        return view('admin.user.show', compact('object'));
    }

    public function edit(Request $request, $id){
        abort_if(actionDenied('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $object = User::find($id);

        return view('admin.user.edit', compact('object'));
    }

    public function update(Request $request, $id){
        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email'],
        ]);

        $object = User::find($id);
        $object->update($request->all());

        return redirect()->route('admin.users.index');
    }

    public function destroy(Request $request, $id){
        abort_if(actionDenied('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $object = User::find($id);
        $object->delete();

        return back();
    }
}
