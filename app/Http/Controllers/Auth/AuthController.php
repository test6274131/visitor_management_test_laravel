<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    use AuthorizesRequests, ValidatesRequests;
    
    public function login(){
        return view('auth.login');
    }

    public function userLogin(Request $request){
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        // dd($request->all());

        if(Auth::attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            return redirect()->route('admin.home');
        }else{
            return back()->withErrors(['account' => trans('global.invalid_email_or_password')]);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect('/login');
    }
}
