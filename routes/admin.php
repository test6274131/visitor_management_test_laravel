<?php

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('', 'HomeController@index')->name('home');

    Route::resource('users', 'UserController');

    Route::post('visitor/{visitor_id}/check-out', 'VisitorController@checkOut')->name('visitor.checkout');
    Route::resource('visitor', 'VisitorController');
});
