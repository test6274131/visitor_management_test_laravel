<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'auth.', 'namespace' => 'Auth'], function () {

    Route::get('login', 'AuthController@login')->name('login');
    Route::post('user_login', 'AuthController@userLogin')->name('user_login');
});

Route::group(['as' => 'auth.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {

    Route::post('logout', 'AuthController@logout')->name('logout');
});